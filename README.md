<h1 align="center"> ms.product </h1> <br>

## Table of Contents

- [Introduction](#Introduction)
- [How to run locally](#How-to-run-locally)
- [Tech stack](#Tech-stack)
- [Swagger](#Swagger-link)

## Introduction

This app's responsibility is to save information about products. 

## How to run locally

shell script
$ java -jar ms.payment.jar


## Tech stack
1. Spring Boot
2. Java 11

## Swagger link
[ms.auth](http://localhost:8081/swagger-ui/index.html#/
