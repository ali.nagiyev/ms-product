package az.product.msproduct.dao.repository;

import az.product.msproduct.dao.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long>{

    List<ProductEntity> findAllByStockCountIsNot(Integer stockCount);

    Optional<ProductEntity> findByProductUuid(String uuid);

    List<ProductEntity> findByProductUuidIn(List<String> uuidList);
}
