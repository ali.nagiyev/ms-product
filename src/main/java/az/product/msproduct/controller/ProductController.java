package az.product.msproduct.controller;

import az.product.msproduct.model.dto.ProductDto;
import az.product.msproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ProductController {

    ProductService productService;

    @GetMapping
    public List<ProductDto> getAllAvailableProducts() {

        return productService.getAllAvailableProducts();
    }

    @GetMapping("/{id}")
    public ProductDto getProductById(
            @PathVariable Long id
    ) {
        return productService.getProductById(id);
    }

    @GetMapping("/uuid")
    public Map<String, ProductDto> getProductByUuid(
            @RequestParam List<String> uuid
    ){
        return productService.getProductByUuid(uuid);
    }

    @PutMapping
    public void checkAndUpdateProductCount(
            @RequestParam String productUuid, @RequestParam Integer productCount
    ) {
        productService.checkAndUpdateProductCount(productUuid, productCount);
    }

    @PutMapping("/reverse")
    public void reverseProduct(
            @RequestParam String productUuid, @RequestParam Integer productCount
    ) {
        productService.reverseProduct(productUuid, productCount);
    }
}
