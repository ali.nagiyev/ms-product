package az.product.msproduct.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Exceptions {

    GENERAL_EXCEPTION(
            "GENERAL_EXCEPTION",
            "Xəta baş verdi",
            "Something went wrong",
            "Что-то пошло не так"
    );

    private final String code;
    private final String translationAz;
    private final String translationEn;
    private final String translationRu;
}