package az.product.msproduct.mapper;

import az.product.msproduct.dao.entity.ProductEntity;
import az.product.msproduct.model.dto.ProductDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto toProductDto(ProductEntity productEntity);

    List<ProductDto> toProductDtoList(List<ProductEntity> productEntityList);
}
